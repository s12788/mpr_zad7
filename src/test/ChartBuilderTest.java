package test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

import domains.*;
import builders.ChartBuilder;

public class ChartBuilderTest {

	@Test
	public void test_withSeries() {
		
		ChartBuilder chartBuilder = new ChartBuilder();
		ChartSerie chartSerie = new ChartSerie("label1", 
		Arrays.asList( new Point(1, 1), new Point(4, 4)), SerieType.Point);
		ChartSettings chartSettings = chartBuilder.withSeries(Collections.singletonList(chartSerie)).build();

		assertThat(chartSettings.getSeries()).hasSize(1);
		assertThat(chartSettings.getSeries()).contains(chartSerie);
	}

	@Test
	public void test_withTitle() {
		
		ChartBuilder chartBuilder = new ChartBuilder();
		String title = "test title";
		ChartSettings chartSettings = chartBuilder.withTitle(title).build();

		assertThat(chartSettings.getTitle()).isEqualTo(title);
	}

	@Test
	public void test_withSubtitle() {
		
		ChartBuilder chartBuilder = new ChartBuilder();
		String subtitle = "test subtitle";
		ChartSettings chartSettings = chartBuilder.withSubtitle(subtitle).build();

		assertThat(chartSettings.getSubtitle()).isEqualTo(subtitle);
	}

	@Test
	public void test_with_haveLegend() {
		
		ChartBuilder chartBuilder = new ChartBuilder();
		ChartSettings chartSettings = chartBuilder.withLegend().build();

		assertThat(chartSettings.isHaveLegend()).isTrue();
	}

	@Test
	public void test_with_Type() {
		
		ChartBuilder chartBuilder = new ChartBuilder();
		ChartType testChartType = ChartType.Line;
		ChartSettings chartSettings = chartBuilder.withType(testChartType).build();

		assertThat(chartSettings.getChartType()).isEqualTo(testChartType);
	}

	@Test
	public void test_without_haveLegend() {
		
		ChartBuilder chartBuilder = new ChartBuilder();
		String title = "test title";
		String subtitle = "test subtitle";
		ChartSettings chartSettings = chartBuilder.withTitle(title).withSubtitle(subtitle).build();

		assertThat(chartSettings.isHaveLegend()).isFalse();
	}
	@Test
	
	public void test_with_addseries() {
		
		ChartBuilder chartBuilder = new ChartBuilder();
		ChartSerie chartSerie = new ChartSerie("label1", 
		Arrays.asList( new Point(1, 1), new Point(4, 4)), SerieType.Point);
		ChartSerie chartSerie2 = new ChartSerie("label2",
		Arrays.asList(new Point(7, 7), new Point(11, 11)), SerieType.Bar);

		ChartSettings chartSettings = chartBuilder.addSerie(chartSerie).addSerie(chartSerie2).build();
		assertThat(chartSettings.getSeries()).hasSize(2);
		assertThat(chartSettings.getSeries()).contains(chartSerie).
		contains(chartSerie2);
	}
	
	@Test
	public void test_with_correct_chartSettings() {
		
		ChartBuilder chartBuilder = new ChartBuilder();
		String title = "test title";
		String subtitle = "test subtitle";
		ChartType chartType = ChartType.Bar;
		ChartSettings chartSettings = chartBuilder.withTitle(title).withSubtitle(subtitle).withType(chartType).withLegend().build();

		assertThat(chartSettings.getTitle()).isEqualTo(title);
		assertThat(chartSettings.getSubtitle()).isEqualTo(subtitle);
		assertThat(chartSettings.getChartType()).isEqualTo(chartType);
		assertThat(chartSettings.isHaveLegend()).isTrue();
	}
}
