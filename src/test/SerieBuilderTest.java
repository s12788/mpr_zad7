package test;

import org.junit.Test;

import domains.*;
import builders.SerieBuilder;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class SerieBuilderTest {



	@Test
	public void test_withPoints() {
		SerieBuilder serieBuilder = new SerieBuilder();
		Point p = new Point(1, 1);
		ChartSerie chartSerie = serieBuilder.
		withPoints(Collections.singletonList(p)).build();

		assertThat(chartSerie.getPoints()).hasSize(1);
		assertThat(chartSerie.getPoints()).contains(p);
	}

	@Test
	public void test_withLabel() {
		SerieBuilder serieBuilder = new SerieBuilder();
		String label = "label";
		ChartSerie chartSerie = serieBuilder.withLabel(label).build();

		assertThat(chartSerie.getLabel()).isEqualTo(label);
	}

	@Test
	public void test_withType() {
		SerieBuilder serieBuilder = new SerieBuilder();
		SerieType serieType = SerieType.Area;
		ChartSerie chartSerie = serieBuilder.withType(serieType).build();

		assertThat(chartSerie.getSerieType()).isEqualTo(serieType);
	}

	@Test
	public void test_with_chartSerie() {
		SerieBuilder serieBuilder = new SerieBuilder();
		String label = "label";
		SerieType testSerieType = SerieType.Bar;
		ChartSerie chartSerie = serieBuilder.withLabel(label).withType(testSerieType).build();

		assertThat(chartSerie.getLabel()).isEqualTo(label);
		assertThat(chartSerie.getSerieType()).isEqualTo(testSerieType);
	}
	
	@Test
	public void test_with_addPoint() {
		SerieBuilder serieBuilder = new SerieBuilder();
		Point p1 = new Point(1, 4);
		Point p2 = new Point(7, 14);
		ChartSerie chartSerie = serieBuilder.addPoint(p1).addPoint(p2).build();

		assertThat(chartSerie.getPoints()).hasSize(2);
		assertThat(chartSerie.getPoints()).contains(p1).contains(p2);
	}
}
