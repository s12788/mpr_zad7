package domains;

public enum SerieType {
    Line, Point, LinePoint, Bar, Area
}
